/*
 *
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @author Sarah Brood   <sarah.brood@ecole.ensicaen.fr>
 * @author Alexis Dupont <alexis.dupont@ecole.ensicaen.fr>
 * @version     1.0.1 - 2020-01-06
 *
 */


#include <stdio.h>
#include "rag.h"

#define size get_RAG_size(r)


/**
*   Cette procédure effectue ittérativement des fusions de régions jusqu'à ce que la fusion de coût minimal soit au délà du seuil
*   @param r le RAG sur lequel effectuer les fusions
*   @param seuil valeur à dépasser pour le coût
*
*/
extern void perform_merge(rag r, double seuil) {
    int i,j, voisin_block;
    double cost;
    cellule voisin;

    voisin = faire_cellule();

    for (i=0; i< size; i++){
        if (get_RAG_father_i(r, i) == i) {
            voisin = get_RAG_neighbours_i(r,i);
            while (voisin != NULL) {
                voisin_block = get_Cellule_block(voisin);
                set_Cellule_block(voisin,get_RAG_father_i(r,voisin_block));
                j = get_Cellule_block(voisin);
                if (get_RAG_father_i(r, j) == j) {
                    cost = calcul_cost(get_RAG_moment_i(r,i), get_RAG_moment_i(r,j));
                    if (seuil >= cost){
                        RAG_merge_region(r, i, j);
                    }
                    voisin= get_cellule_next(voisin);
                }
            }
        }
    }

    RAG_normalize_parents(r);
    FREE_CELLULE(voisin);
}

/**
*   Cette fonction créé une image où chaque block est affiché avec la couleur moyenne de son block parent
*   @param r le RAG pour créer l'image
*
*   @return l'image de sortie
*/
extern image create_output_image(rag r ){
    int * average_color;
    int i, j, h, l, n, m, count_i, count_j, block, image_dim;
    image output = FAIRE_image();
    image img = RAG_give_image(r);
    h = image_give_hauteur(img);
    l = image_give_largeur(img);
    n = RAG_give_nb_block_per_line(r);
    m = RAG_give_nb_block_per_column(r);
    image_dim = image_give_dim(img);

    average_color =(int*) malloc(sizeof(int)*image_dim);
    count_i, count_j = 0;
    block = 0;
    i = 0;
    image_initialize(output,image_dim, l, h) ;

    while(block < get_RAG_size(r)) {
      RAG_give_mean_color(r, block,average_color);
      i = h/m * (block/n);
      j = l/n * (block%n);
      count_j = 0;

      while (count_j < l/n) {
        i = h/m * (block/n);
        count_i = 0;

        while (count_i < h/m) {
          image_write_pixel_bis(output, i, j, average_color);
          count_i++;
          i++;
        }
        count_j++;
        j++;
      }
      block++;
    }
    free(average_color);
    free(img);
    return output;
}
