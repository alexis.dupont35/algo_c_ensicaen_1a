/*
 *
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @author Sarah Brood   <sarah.brood@ecole.ensicaen.fr>
 * @author Alexis Dupont <alexis.dupont@ecole.ensicaen.fr>
 * @version     1.0.1 - 2020-01-06
 *
 */


#include <stdio.h>
#include "include/image.h"
#include "include/rag.h"
#include "include/projet.h"


int main(int argc, char *argv[]){
    rag rag;
    image image, output;
    int nb_block_ligne, nb_block_column, seuil;

    nb_block_ligne = atoi(argv[2]);
    nb_block_column = atoi(argv[3]);
    seuil = atof(argv[4]);

    image = FAIRE_image();
    image_charger(image,argv[1]);

    rag = create_RAG(image,nb_block_ligne, nb_block_column);
    perform_merge(rag, seuil);
    output = FAIRE_image();
    output = create_output_image(rag);
    image_to_stream(output,stdout);

    DEFAIRE_image(output);
    DEFAIRE_image(image);
    FREE_RAG(rag);
    return 0;
}
