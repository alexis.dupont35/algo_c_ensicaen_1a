#ifndef RAG_H
#define RAG_H

#include "utils/objet.h"
#include "utils/classe.h"

#include "image.h"

//CREATIONS MOMENTS & RAG
CLASSE(rag);
CLASSE(moments);
CLASSE(cellule);

// CALCULS
extern double     calcul_cost(moments,moments);
extern double     RAG_give_closest_region(rag,int*,int*);
extern void       RAG_merge_region(rag,int,int);
extern void       RAG_normalize_parents(rag);
extern void       RAG_give_mean_color(rag,int,int*);

// INSTANCEURS
extern rag        create_RAG(image,int,int);
extern cellule    faire_cellule();


// ACCESSEURS - attributs de rag
extern int        get_RAG_size(rag);
extern int *      get_RAG_father(rag);
extern cellule *  get_RAG_neighbours(rag);
extern int        RAG_give_nb_block_per_line(rag);
extern int        RAG_give_nb_block_per_column(rag);
extern image      RAG_give_image(rag);

// ACCESSEURS - i ème élément d'un attribut de rag
extern int       get_RAG_father_i(rag,int);
extern cellule   get_RAG_neighbours_i(rag,int);
extern moments   get_RAG_moment_i(rag,int);

// ACCESSEURS - attributs cellule
extern cellule   get_cellule_next(cellule);
extern int       get_Cellule_block(cellule);

// SETTEUR - attribut cellule
extern void      set_Cellule_block(cellule,int);

extern void      FREE_RAG(rag);
extern void      FREE_CELLULE(cellule);

#endif /* RAG_H */
