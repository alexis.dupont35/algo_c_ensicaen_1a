/*
 *
 * ENSICAEN
 * 6 Boulevard Maréchal Juin
 * F-14050 Caen Cedex
 *
 * This file is owned by ENSICAEN students.
 * No portion of this document may be reproduced, copied
 * or revised without written permission of the authors.
 */

/**
 * @author Sarah Brood   <sarah.brood@ecole.ensicaen.fr>
 * @author Alexis Dupont <alexis.dupont@ecole.ensicaen.fr>
 * @version     1.0.1 - 2020-01-06
 *
 */


#include <stdio.h>
#include "utils/type_obj.h"
#include "rag.h"
#include "image.h"

//ACCESSEUR ATTRIBUTS RAG
#define Size                    SELF(size)
#define Img                     SELF(img)
#define Moments                 SELF(moments)
#define Father                  SELF(father)
#define Neighbors               SELF(neighbors)
#define Block_per_line          SELF(block_per_line)
#define Block_per_column        SELF(block_per_column)

//FONCTIONS STATIQUES
static void    give_moments(image im, int num_block,int n, int m, int *m0, double* m1, double* m2);
static cellule neighbors(int block,int n, int size);
static int*    RAG_give_neighboors(rag r, int num_block, int * blocks);
static void    initialize_cell(cellule c, int block, cellule next);
static void    initialize_moment(moments m, image img);
static void    add_neighbor(cellule c1, cellule c2);
static void    RAG_merge(rag self, int b1, int b2);


//STRUCTURES
struct moments
{
    int     M0;
    double *M1;
    double *M2;
};

struct cellule
{
    int     block;
    cellule next;
};

struct rag
{
    int       size;
    int       block_per_line;
    int       block_per_column;
    image     img;
    moments * moments;
    int *     father;
    cellule * neighbors;

};

//INSTANCEURS
rag faire_RAG(){
    INSTANCIER(rag);

    return self;
}

moments faire_moments(){
    INSTANCIER(moments);

    return self;
}

cellule faire_cellule(){
    INSTANCIER(cellule);

    return self;
}

//INITIALISATIONS
static void initialize_moment(moments m, image img){
    m->M1=malloc(sizeof(int)*image_give_dim(img));
    m->M2=malloc(sizeof(int)*image_give_dim(img));
}

static void initialize_cell(cellule c, int block, cellule next){
    c->block=block;
    c->next=next;
}

//FONCTIONS EXTERNES

/**
* Cette fonction permet de créer le graphe d'adjacence des régions d'une image (block adjacents)
*
* @param img l'image à partir de laquelle le graphe doit être créé
* @param n nombre de blocks par ligne
* @param m nombre de blocks par colonne
* @return rag graphe
*/

extern rag create_RAG(image img,int n,int m) {
    assert(n>=0 && m>=0 && img !=NULL);

    int i;
    rag self = faire_RAG();

    Size = n * m;
    Block_per_line = n;
    Block_per_column = m;
    Img = img;
    Father = (int*)malloc(sizeof(int)*(Size));
    Moments =  malloc(sizeof(moments)*(Size));
    Neighbors = malloc(sizeof(cellule)*(Size));

    for (i = 0; i < Size; i++) {
        Father[i] = i;
        Moments[i]  = faire_moments();
        initialize_moment(Moments[i], img);
        give_moments(img, i, Block_per_line, Block_per_column, &(Moments[i]->M0), Moments[i]->M1, Moments[i]->M2);
        Neighbors[i] = malloc(sizeof(cellule));
        Neighbors[i] = neighbors(i,n,Size);
    }

    return self;
}


/**
* Cette fonction permet de calculer quels sont les deux blocks dont la fusion induit la plus petite augmentation d'erreur quadratique
*
* @param self graphe d'adjacence des régions
* @param block1 à envoyer pour récupérer le numéro du block 1
* @param block2 à envoyer pour récupérer le numéro du block 2
* @return l'augmentation de l'erreur quadratique de ceux deux blocks
*/
extern double RAG_give_closest_region(rag self,int* block1, int* block2){
    int i,j;
    double min, cost;
    cellule voisin = faire_cellule();

    min = calcul_cost(Moments[0], Moments[1]);
    *block1 = 0;
    *block2 = 1;

    for (i=0; i<Size; i++){
        if (Father[i] == i) {
            voisin = Neighbors[i];
            while (voisin != NULL) {
                voisin -> block = Father[voisin->block];
                j = voisin->block;
                if (Father[j] == j) {
                    cost = calcul_cost(Moments[i], Moments[j]);
                    if (min > cost) {
                        min = cost;
                        *block1 = i;
                        *block2 = j;
                    }
                }
                voisin = voisin->next;
            }
        }
    }
    return min;
}

/**
* Cette fonction permet de calculer l'erreur quadratique des deux moments, correspond à l'homogénéité des deux blocks
*
* @param moment1
* @param moment2
* @return l'erreur quadratique des deux moments
*/

extern double calcul_cost(moments moment1, moments moment2) {
    double norm;
    double red, green, blue;
    double cost = (moment1->M0 * moment2->M0) / (moment1->M0 + moment2->M0);

    red = moment1->M1[0]/moment1->M0 - moment2->M1[0]/moment2->M0;
    green = moment1->M1[1]/moment1->M0 - moment2->M1[1]/moment2->M0;
    blue = moment1->M1[2]/moment1->M0 - moment2->M1[2]/moment2->M0;
    norm = red*red + green*green + blue*blue;
    cost = cost * norm;

    return cost;
}

/**
* Cette procédure permet de calculer les moments d'odre 0,1 et 2 des pixels situés dans un rectangle.
*
* @param image l'image dont les moments doivent être calculés
* @param num_block numéro du block définissant le rectangle
* @param n nombre de blocks par ligne
* @param m nombre de blocks par colonne
* @param m0 moment d'ordre 0 (nombre de pixels dans le block)
* @param m1 moment d'ordre 1 (somme des couleurs des pixels dans le block)
* @param m2 moment d'ordre 2 (somme des couleurs des pixels dans le block au carré )
*/

static void give_moments(image image, int num_block,int n, int m, int *m0, double* m1, double* m2){
    int i,h, l , h_block, l_block, px_block, py_block, largeur,hauteur;
    Point p;
    int * pix;

    hauteur = image_give_hauteur(image);
    largeur = image_give_largeur(image);
    h_block = hauteur/m; /* hauteur block */
    l_block = largeur/n; /* largeur block */
    px_block = l_block* (num_block%n);/*ligne en haut à gauche du block*/
    py_block = h_block * (num_block/n); /*colonne en haut à gauche du block*/
    *m0 = h_block * l_block; /*moment d'ordre 0 : nombre de pixels dans le block */
    COORDX(p)= px_block;
    COORDY(p)= py_block;

    for(h = py_block; h < py_block + h_block; h++){
        for(l = px_block; l < px_block + l_block; l++){
            COORDX(p)= l;
            COORDY(p)= h;
            image_move_to(image, &p); /* on pointe l'image sur le pixel de coordonnées p.x et p.y */
            pix = image_lire_pixel(image); /* on récupère le pixel courant */
            if(image_give_dim(image)>=0){
                for(i = 0; i < 3; i++){
                    m1[i] += pix[i]; /* somme des couleurs des pixels */
                    m2[i] += pix[i] * pix[i]; /* somme des couleurs des pixels au carrés */
                }
            }else{
                m1 +=pix[0];
                m2 +=pix[0] * pix[0];
            }
        }
    }
}

/**
* Cette procédure fusionne les deux blocks par la mise à jour du tableau de père, la mise à jour pa sommation des moments et la fusion des voisins
*
* @param rag graphe à mettre à jour
* @param b1 block à fusionner
* @param b2 block à fusionner
* @return l'erreur quadratique des deux moments
*/
extern void RAG_merge_region(rag self, int b1, int b2){
    assert(b1!=b2);
    int i;
    if(b1<b2){
        RAG_merge(self,b1,b2);
    }
    else{
        RAG_merge(self, b2,b1);
    }
}

static void RAG_merge(rag self, int b1, int b2){
    cellule c,tmp, next_tmp;
    int i;
    Neighbors[b1] = NULL;
    Father[b1] = b2;
    Moments[b1]->M0 += Moments[b2]->M0;
    for(i = 0; i<image_give_dim(Img); i++){
        Moments[b1]->M1[i] += Moments[b2]->M1[i];
        Moments[b1]->M2[i] += Moments[b2]->M2[i];
    }
    c = Neighbors[b1];
    while(c!=NULL){
        if(c->block != b2) add_neighbor(c,Neighbors[b2]);
        c = Neighbors[b1]->next;
    }
    Neighbors[b1] = NULL;
}

/**
* Cette procédure ajoute les voisins de la cellule c1 aux voisins de la cellule c2
*
*/
static void add_neighbor(cellule c1, cellule c2){
    cellule tmp;
    cellule tmp_next;
    tmp = faire_cellule();
    tmp_next = faire_cellule();
    if(c2!=NULL){
        if(c2->block < c1->block){
            c2->next->block < c1->block ? initialize_cell(tmp,c1->block,NULL) : initialize_cell(tmp,c1->block,c2->next);
            c2->next = tmp;
        }else{
            initialize_cell(tmp,c1->block,c2);
            c2 = tmp;
        }
    }else{
        initialize_cell(c2, c1->block,NULL);
    }
}

/**
*   Initialise les voisins du block
*
*/
static cellule neighbors(int block,int n, int size){
    cellule voisin1, voisin2;
    voisin1 = malloc(sizeof(struct cellule));
    voisin2 = malloc(sizeof(struct cellule));
    if ((block+1) % n == 0) {
        if ((size)-(block+1) < n) {

            return NULL;
        }
        else {
            voisin1->block = block + n;
            voisin1->next = NULL;

            return voisin1;
        }
    }
    else{
        if ((size)-(block+1) < n) {
            voisin1->block = block+1;
            voisin1->next = NULL;

            return voisin1;
        }
        else {
            voisin1->block = block+1;
            voisin2->block = block + n;
            voisin2->next = NULL;
            voisin1->next = voisin2;

            return voisin1;
        }
    }
}


/**
*   Cette procédure calcule la couleur moyenne d'un block
*   @param rag le graphe
*   @param num_block le numéro du block
*   @param average_color pointeur pour retourner la moyenne -de dim 1 ou 3-
*/
extern void RAG_give_mean_color(rag self,int num_block,int* average_color){
    int i;
    num_block = Father[num_block];
    if(image_give_dim(Img) >= 0){
        for(i = 0; i < 3; i++){
            average_color[i] = Moments[num_block]->M1[i]/Moments[num_block]->M0;
        }
    }else{
        *average_color = Moments[num_block]->M1[0] / Moments[num_block]->M0;
    }
}

/**
*   Cette procédure supprime les indirections causées par perform_merge
*   @param le graphe à parcourir
*/
extern void RAG_normalize_parents(rag self){
    int i;
    for (i=Size-1; i>=0; i--){
        Father[i] = Father[Father[i]];
    }
}

//DE-ALLOCATION
extern void FREE_RAG(rag self){
    free(Moments);
    free(Father);
    free(Neighbors);
    free(self);
}

static void FREE_MOMENTS(moments m){
    free(m->M1);
    free(m->M2);
    free(m);
}

extern void FREE_CELLULE(cellule c){
    free(c);
}

//ACCESSEURS RAG
extern int get_RAG_size(rag self){
    return Size;
}

extern moments * get_RAG_moments(rag self){
    return Moments;
}

extern cellule * get_RAG_neighbours(rag self){
    return Neighbors;
}

extern int * get_RAG_father(rag self){
    return Father;
}

extern image RAG_give_image(rag self){
    return Img;
}

extern int RAG_give_nb_block_per_line(rag self){
    return Block_per_line;
}

extern int RAG_give_nb_block_per_column(rag self){
    return Block_per_column;
}

// ACCESSEURS  - i ème élément d'un attribut de rag
extern cellule get_RAG_neighbours_i(rag self , int i){
    return Neighbors[i];
}

extern int get_RAG_father_i(rag self, int i ){
    return Father[i];
}

extern moments get_RAG_moment_i(rag self,int i){
    return Moments[i];
}

//ACCESSEURS - attributs cellule
extern int get_Cellule_block(cellule c){
    return c->block;
}

extern cellule get_cellule_next(cellule c){
    return c->next;
}

// SETTEUR - attribut cellule
extern void set_Cellule_block(cellule c,int i){
    c->block=i;
}
