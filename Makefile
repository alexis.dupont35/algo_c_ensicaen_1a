CC= gcc
CFLAG= -Wall -g
LIBS= -L lib/ -limage64
OBJ= main.o
OUTPUT= testimage
INCLUDE= include
UTILS= include/utils

$(OUTPUT): $(OBJ)
	$(CC) $(OBJ) -o $@ $(LIBS)

clean:
	rm -f $(OUTPUT) $(OBJ)

main.o: main.c $(INCLUDE) $(UTILS)
	$(CC) $(CFLAG) -c main.c
